//
//  ViewController.swift
//  Contact Manager
//
//  Created by H. Türkü Kaya on 12/04/15.
//  Copyright (c) 2015 H. Türkü Kaya. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var profileIw: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet weak var surnameTf: UITextField!
    @IBOutlet weak var companyTf: UITextField!
    @IBOutlet weak var numberTf: UITextField!
    @IBOutlet weak var mailTf: UITextField!
    @IBOutlet weak var addNumberBtn: UIButton!
    @IBOutlet weak var addmailBtn: UIButton!
    
    var yPosition:CGFloat = 0
    var xPosition:CGFloat = 0
    var tfHeight:CGFloat = 0
    var tfWidth:CGFloat = 0
    
    var screenRect:CGRect = UIScreen.mainScreen().bounds
    var screenWidth:CGFloat = 0;
    var screenHeight:CGFloat = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        var c:Contact = Contact(name: "Umut", surname: "Kaya", company: "", email: "", number: "", note: "", image: "")
//        c.create()
        
        var btn:UIBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("deneme"))
        self.navigationItem.rightBarButtonItem = btn
        yPosition = numberTf.frame.origin.y + 35
        xPosition = numberTf.frame.origin.x
        tfWidth = numberTf.frame.size.width
        tfHeight = numberTf.frame.size.height
        
        screenWidth = screenRect.size.width;
        screenHeight = screenRect.size.height
        
        println(screenWidth)
        println(screenHeight)
    }
    
    @IBAction func addNumber(sender: AnyObject) {
        println("asd")
        var tf = UITextField(frame: CGRectMake((xPosition + 3), yPosition, (screenWidth - 52), tfHeight))
        tf.frame.origin.y = yPosition
        tf.backgroundColor = numberTf.backgroundColor
        tf.borderStyle = numberTf.borderStyle
        tf.font = numberTf.font
        tf.textAlignment = numberTf.textAlignment
        var dhz = mailTf.frame
        var const = mailTf.constraints()
        mailTf.frame.origin.y += 35
        addmailBtn.frame.origin.y += 35
        yPosition += 35
        tf.placeholder = "Numara"
        self.scrollView.addSubview(tf)
        resizeScroll()
    }

    

    @IBAction func addMail(sender: AnyObject) {
        var x:String = "sdas"
    }
    
    
    func deneme() {
        var views = self.childViewControllers
        for view in views {
            var a:UIViewController = view as! UIViewController
            var sl = "asdas"
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        screenWidth = screenRect.size.width;
        resizeScroll()
    }
    
    func resizeScroll() {
        var scrollViewHeight:CGFloat = 0.0;
        for view in scrollView.subviews {
            var v:UIView = view as! UIView
            scrollViewHeight += v.frame.size.height;
        }
        scrollView.contentSize.width = screenWidth
        scrollView.contentSize.height = scrollViewHeight + 50
    }


}

