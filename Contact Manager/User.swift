//
//  User.swift
//  Contact Manager
//
//  Created by H. Türkü Kaya on 12/04/15.
//  Copyright (c) 2015 H. Türkü Kaya. All rights reserved.
//

import UIKit
import CoreData

class User {
    var id: String? = ""
    var name: String? = ""
    var surname: String? = ""
    var email: String? = ""
    var password: String? = ""
    var token: String? = ""
    var avatar: String? = ""
    var party: String? = ""
    var city: String? = ""
    var phone: String? = ""
    var title: String? = ""
    
    private var appDel:AppDelegate
    private var context:NSManagedObjectContext
    
    init(name: String, surname: String, email: String, password: String, token: String, avatar: String, party: String, city: String, phone: String, title: String)
    {
        appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        context = appDel.managedObjectContext!
        
        self.id = NSUUID().UUIDString
        self.name = name
        self.surname = surname
        self.email = email
        self.password = password
        self.token = token
        self.avatar = avatar
        self.party = party
        self.city = city
        self.phone = phone
        self.title = title
    }
    
    init(id: String, name: String, surname: String, email: String, password: String, token: String, avatar: String, party: String, city: String, phone: String, title: String)
    {
        appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        context = appDel.managedObjectContext!
        
        self.id = id
        self.name = name
        self.surname = surname
        self.email = email
        self.password = password
        self.token = token
        self.avatar = avatar
        self.party = party
        self.city = city
        self.phone = phone
        self.title = title
    }
    
    func create() {
        var newUser = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: context) as! NSManagedObject
        
        newUser.setValue(self.id!, forKey: "id")
        newUser.setValue(self.name!, forKey: "name")
        newUser.setValue(self.surname!, forKey: "surname")
        newUser.setValue(self.email!, forKey: "email")
        newUser.setValue(self.password!, forKey: "password")
        newUser.setValue(self.token!, forKey: "token")
        newUser.setValue(self.avatar!, forKey: "avatar")
        newUser.setValue(self.party!, forKey: "party")
        newUser.setValue(self.city!, forKey: "city")
        newUser.setValue(self.phone!, forKey: "phone")
        newUser.setValue(self.title!, forKey: "title")
        
        context.save(nil)
    }
    
    func update() {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "User")
        request.predicate = NSPredicate(format: "id = %@", self.id!)
        
        var results: [AnyObject]? = context.executeFetchRequest(request, error: nil)
        if results?.count > 0 {
            for result in results! {
                result.setValue(self.id!, forKey: "id")
                result.setValue(self.name!, forKey: "name")
                result.setValue(self.surname!, forKey: "surname")
                result.setValue(self.email!, forKey: "email")
                result.setValue(self.password!, forKey: "password")
                result.setValue(self.token!, forKey: "token")
                result.setValue(self.avatar!, forKey: "avatar")
                result.setValue(self.party!, forKey: "party")
                result.setValue(self.city!, forKey: "city")
                result.setValue(self.phone!, forKey: "phone")
                result.setValue(self.title!, forKey: "title")
                context.save(nil)
            }
        }
    }
    
    func remove() {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "User")
        request.predicate = NSPredicate(format: "id = %@", self.id!)
        
        var results: [AnyObject]? = context.executeFetchRequest(request, error: nil)
        if results?.count > 0 {
            for result in results! {
                context.deleteObject(result as! NSManagedObject)
                context.save(nil)
            }
        }
    }
    
    func print() {
        if self.id != nil {
            println("ID: \(self.id!) - Ad: \(self.name!) - Soyad: \(self.surname!)")
        } else {
            println("Bulunamadı!")
        }
    }
    
    /* STATIC FINDER METHODS */
    
    static func all() -> [User] {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var arr = [User]()
        
        var request = NSFetchRequest(entityName: "User")
        
        var results = context.executeFetchRequest(request, error: nil)
        
        if results?.count > 0 {
            for result in results! {
                var _id: String = result.valueForKey("id") as! String
                var _name: String = result.valueForKey("name") as! String
                var _surname: String = result.valueForKey("surname") as! String
                var _email: String = result.valueForKey("email") as! String
                var _password: String = result.valueForKey("password") as! String
                var _token: String = result.valueForKey("token") as! String
                var _avatar: String = result.valueForKey("avatar") as! String
                var _party: String = result.valueForKey("party") as! String
                var _city: String = result.valueForKey("city") as! String
                var _phone: String = result.valueForKey("phone") as! String
                var _title: String = result.valueForKey("title") as! String
                var user = User(id: _id, name: _name, surname: _surname, email: _email, password: _password, token: _token, avatar: _avatar, party: _party, city: _city, phone: _phone, title: _title)
                arr.append(user)
                
            }
            
        }
        
        return arr
    }
    
    static func find(id: String) -> User? {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "User")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        var results: [AnyObject]? = context.executeFetchRequest(request, error: nil)
        if results?.count > 0 {
            for result in results! {
                var _id: String = result.valueForKey("id") as! String
                var _name: String = result.valueForKey("name") as! String
                var _surname: String = result.valueForKey("surname") as! String
                var _email: String = result.valueForKey("email") as! String
                var _password: String = result.valueForKey("password") as! String
                var _token: String = result.valueForKey("token") as! String
                var _avatar: String = result.valueForKey("avatar") as! String
                var _party: String = result.valueForKey("party") as! String
                var _city: String = result.valueForKey("city") as! String
                var _phone: String = result.valueForKey("phone") as! String
                var _title: String = result.valueForKey("title") as! String
                return User(id: _id, name: _name, surname: _surname, email: _email, password: _password, token: _token, avatar: _avatar, party: _party, city: _city, phone: _phone, title: _title)
            }
        }
        
        return nil
    }
    
    static func search(term: String) -> [User] {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "User")
        request.predicate = NSPredicate(format: "(name CONTAINS[cd] %@) OR (surname CONTAINS[cd] %@) OR (email CONTAINS[cd] %@) OR (email CONTAINS[cd] %@) OR (party CONTAINS[cd] %@) OR (city CONTAINS[cd] %@ OR (phone CONTAINS[cd] %@ OR (title CONTAINS[cd] %@)", term, term, term, term, term, term, term, term)
        var arr = [User]()
        var results: [AnyObject]? = context.executeFetchRequest(request, error: nil)
        if results?.count > 0 {
            for result in results! {
                var _id: String = result.valueForKey("id") as! String
                var _name: String = result.valueForKey("name") as! String
                var _surname: String = result.valueForKey("surname") as! String
                var _email: String = result.valueForKey("email") as! String
                var _password: String = result.valueForKey("password") as! String
                var _token: String = result.valueForKey("token") as! String
                var _avatar: String = result.valueForKey("avatar") as! String
                var _party: String = result.valueForKey("party") as! String
                var _city: String = result.valueForKey("city") as! String
                var _phone: String = result.valueForKey("phone") as! String
                var _title: String = result.valueForKey("title") as! String
                var user: User = User(id: _id, name: _name, surname: _surname, email: _email, password: _password, token: _token, avatar: _avatar, party: _party, city: _city, phone: _phone, title: _title)
                
                arr.append(user)
            }
        }
        
        return arr
    }

}
