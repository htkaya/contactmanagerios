//
//  Contact.swift
//  Contact Manager
//
//  Created by H. Türkü Kaya on 12/04/15.
//  Copyright (c) 2015 H. Türkü Kaya. All rights reserved.
//

import UIKit
import CoreData

class Contact {
    var id: String? = ""
    var name: String? = ""
    var surname: String? = ""
    var company: String? = ""
    var email: String? = ""
    var number: String? = ""
    var note: String? = ""
    var image: String? = ""
    
    private var appDel:AppDelegate
    private var context:NSManagedObjectContext
    
    init(name: String, surname: String, company: String, email: String, number: String, note: String, image: String)
    {
        appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        context = appDel.managedObjectContext!
        
        self.id = NSUUID().UUIDString
        self.name = name
        self.surname = surname
        self.company = company
        self.email = email
        self.number = number
        self.note = note
        self.image = image
    }
    
    init(id: String, name: String, surname: String, company: String, email: String, number: String, note: String, image: String)
    {
        appDel = UIApplication.sharedApplication().delegate as! AppDelegate
        context = appDel.managedObjectContext!
        
        self.id = id
        self.name = name
        self.surname = surname
        self.company = company
        self.email = email
        self.number = number
        self.note = note
        self.image = image
    }
    
    func create() {
        var newContact = NSEntityDescription.insertNewObjectForEntityForName("Contact", inManagedObjectContext: context) as! NSManagedObject
        
        newContact.setValue(self.id!, forKey: "id")
        newContact.setValue(self.name!, forKey: "name")
        newContact.setValue(self.surname!, forKey: "surname")
        newContact.setValue(self.company!, forKey: "company")
        newContact.setValue(self.email!, forKey: "email")
        newContact.setValue(self.number!, forKey: "number")
        newContact.setValue(self.note!, forKey: "note")
        newContact.setValue(self.image!, forKey: "image")
        
        context.save(nil)
    }
    
    func update() {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "Contact")
        request.predicate = NSPredicate(format: "id = %@", self.id!)
        
        var results: [AnyObject]? = context.executeFetchRequest(request, error: nil)
        if results?.count > 0 {
            for result in results! {
                result.setValue(self.id!, forKey: "id")
                result.setValue(self.name!, forKey: "name")
                result.setValue(self.surname!, forKey: "surname")
                result.setValue(self.company!, forKey: "company")
                result.setValue(self.email!, forKey: "email")
                result.setValue(self.number!, forKey: "number")
                result.setValue(self.note!, forKey: "note")
                result.setValue(self.image!, forKey: "image")
                context.save(nil)
            }
        }
    }
    
    func remove() {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "Contact")
        request.predicate = NSPredicate(format: "id = %@", self.id!)
        
        var results: [AnyObject]? = context.executeFetchRequest(request, error: nil)
        if results?.count > 0 {
            for result in results! {
                context.deleteObject(result as! NSManagedObject)
                context.save(nil)
            }
        }
    }
    
    func print() {
        if self.id != nil {
            println("ID: \(self.id!) - Ad: \(self.name!) - Soyad: \(self.surname!)")
        } else {
            println("Bulunamadı!")
        }
    }
    
    /* STATIC FINDER METHODS */
    
    static func all() -> [Contact] {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var arr = [Contact]()
        
        var request = NSFetchRequest(entityName: "Contact")
        
        var results = context.executeFetchRequest(request, error: nil)
        
        if results?.count > 0 {
            for result in results! {
                var _id: String = result.valueForKey("id") as! String
                var _name: String = result.valueForKey("name") as! String
                var _surname: String = result.valueForKey("surname") as! String
                var _company: String = result.valueForKey("company") as! String
                var _email: String = result.valueForKey("email") as! String
                var _number: String = result.valueForKey("number") as! String
                var _note: String = result.valueForKey("note") as! String
                var _image: String = result.valueForKey("image") as! String
                var contact = Contact(id: _id, name: _name, surname: _surname, company: _company, email: _email, number: _number, note: _note, image: _image)
                arr.append(contact)
                
            }
            
        }
        
        return arr
    }
    
    static func find(id: String) -> Contact? {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "Contact")
        request.predicate = NSPredicate(format: "id = %@", id)
        
        var results: [AnyObject]? = context.executeFetchRequest(request, error: nil)
        if results?.count > 0 {
            for result in results! {
                var _id: String = result.valueForKey("id") as! String
                var _name: String = result.valueForKey("name") as! String
                var _surname: String = result.valueForKey("surname") as! String
                var _company: String = result.valueForKey("company") as! String
                var _email: String = result.valueForKey("email") as! String
                var _number: String = result.valueForKey("number") as! String
                var _note: String = result.valueForKey("note") as! String
                var _image: String = result.valueForKey("image") as! String
                return Contact(id: _id, name: _name, surname: _surname, company: _company, email: _email, number: _number, note: _note, image: _image)
            }
        }
        
        return nil
    }
    
    static func search(term: String) -> [Contact] {
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        var request = NSFetchRequest(entityName: "Contact")
        request.predicate = NSPredicate(format: "(name CONTAINS[cd] %@) OR (surname CONTAINS[cd] %@) OR (company CONTAINS[cd] %@) OR (email CONTAINS[cd] %@) OR (number CONTAINS[cd] %@) OR (note CONTAINS[cd] %@)", term, term, term, term, term, term)
        var arr = [Contact]()
        var results: [AnyObject]? = context.executeFetchRequest(request, error: nil)
        if results?.count > 0 {
            for result in results! {
                var _id: String = result.valueForKey("id") as! String
                var _name: String = result.valueForKey("name") as! String
                var _surname: String = result.valueForKey("surname") as! String
                var _company: String = result.valueForKey("company") as! String
                var _email: String = result.valueForKey("email") as! String
                var _number: String = result.valueForKey("number") as! String
                var _note: String = result.valueForKey("note") as! String
                var _image: String = result.valueForKey("image") as! String
                var contact: Contact = Contact(id: _id, name: _name, surname: _surname, company: _company, email: _email, number: _number, note: _note, image: _image)
                
                arr.append(contact)
            }
        }
        
        return arr
    }
}
